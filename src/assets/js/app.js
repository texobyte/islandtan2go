$(document).foundation();

//mobile-menu hiding
//Hide menu if scroll
$(window).scroll(function() {
    if ($(this).scrollTop() > 5 & $('#mobile-menu').css('display') == 'block') { //use `this`, not `document`
        $('#mobile-menu').css({
            'display': 'none'
        });
    }
});

//Hide menu if link item clicked
$("#mobile-menu li").click(function () {
  $('#mobile-menu').css('display','none');
});

//Hide menu if clicked off of
// $(document).on('click', function(event) {
//   if (!$(event.target).closest('#mobile-menu').length) {
//     // Hide the menus.
//     $('#mobile-menu').css('display','none');
//   }
// });

// $(".selector").validate({
//   submitHandler: function(form) {
//     // do other things for a valid form
//     form.submit();
//   }
// });

//booking form pop-up-thank you
// $( 'button[name="bookingForm"]' ).on('click', function(e) {
//     // form validation passed, form will submit if submit event not returned false
//   $('#booking-form-thank').foundation('open');
//   console.log("submitted");
//   // event.preventDefault();;
// });

$('#booking-form').submit(function(e) {
      var fname = $('#firstName')
      var lname = $('#lastName')
      var phone = $('#phone')
      var email = $('#email')
      var service = $('#service')
      var date1 = $('#datepicker-appt')
      var date2 = $('#datepicker-appt2')
      var message = $('#inputMessage')

      if(fname.val() == "" || lname.val() == "" || phone.val() == "" || email.val() == "" || service.val() == "" || date1.val() == "" || message.val() == "") {
        // $('.submit-fail').fadeToggle(400);
        return false;
      }
      else {
        $.ajax({
          method: 'POST',
          // url: '//formspree.io/lance@texobyte.com',
          url: '//formspree.io/karin@islandtan2go.com',
          data: $('#booking-form').serialize(),
          datatype: 'jsonp',
        });
        e.preventDefault();
        // $(this).get(0).reset();
        console.log('success');
        $('#booking-form').foundation('destroy');
        // $('#booking-form').foundation('close');
        $('#booking-form-thank').foundation('open');
      }
    });


//show and hide based on region
$.get("http://freegeoip.net/json/", function (response) {
    $("#ip").html("IP: " + response.ip);
    $("#country_code").html(response.country_code);
    if(response.time_zone=='America/New_York'){
    	$( ".hi" ).remove();
      $( ".ct" ).show().add();
	}
}, "jsonp");

//datepicker
// $( function() {
//     $( "#datepicker-appt" ).datepicker();
//   } );

$(function(){
  $('#datepicker-appt, #datepicker-appt2').fdatepicker({
		format: 'mm-dd-yyyy hh:ii',
		disableDblClickSelection: true,
		language: 'vi',
		pickTime: true
	});
});
